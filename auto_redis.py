#!/usr/bin/env python
# coding=utf-8
import redis
r = redis.Redis(host='127.0.0.1', port=6379 ,db=0)
r.set('test','mysql')
print(r.get('test'))
r.set('db',20)
print(r.get('db'))
r.incr('db')
print(r.get('db'))
r.hset('web','goole',20)
print(r.hget('web','goole').decode('utf-8'))
r.hincrby('web','goole')
print(r.hget('web','goole').decode('utf-8'))
