#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import requests
from flask import Flask
from flask import request
from flask import redirect
from flask import jsonify
app = Flask(__name__)
@app.route('/log' , methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		a = request.get_data()
		dict1 = json.loads(a.decode('utf-8'))
		#print(dict1['videoid'],dict1['ip'])
		l={"status": False, "videoid": dict1['videoid'],"ip": dict1['ip']}
		
		#print(l)
		#return json.dumps(dict1)
		return json.dumps(l)
	else:
		return '<h1>只接受post请求！</h1>'
#@app.route('/user/<name>')
#def user(name):
#	return'<h1>hello, %s</h1>' % name
if __name__ =='__main__':
	app.run(host='127.0.0.1',port=4003,debug=True)
