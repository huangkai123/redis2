							127.0.0.1:6379 学习笔记

一. Key（键）先学 DEL, EXISTS, EXPIRE*, KEYS,RANDOMKEY TYPE

1. DEL key [key ...]

删除给定的一个或多个 key 。
不存在的 key 会被忽略

例如：#  删除单个 key
127.0.0.1:6379> set name xianming
OK
127.0.0.1:6379> del name
(integer) 1
## 删除多个 key
127.0.0.1:6379> set name 127.0.0.1:6379
OK
127.0.0.1:6379> set web mysql
OK
127.0.0.1:6379> del name mysql
(integer) 1
################
2. EXISTS key
检查给定 key 是否存在
返回值：
    若 key 存在，返回 1 ，否则返回 0 。
例如：
27.0.0.1:6379> set test hello
OK
127.0.0.1:6379> exists db
(integer) 0
127.0.0.1:6379> exists test
(integer) 1
###########
3. EXPIRE
例如：
127.0.0.1:6379> set db baidu
OK
127.0.0.1:6379> expire db 30 # 设置过期时间为 30 秒
(integer) 1
127.0.0.1:6379> ttl db # # 查看剩余生存时间
(integer) 21
127.0.0.1:6379> expire db 60 # 更新过期时间
(integer) 1
127.0.0.1:6379> ttl db
(integer) 53
########################
4.KEYS
查找所有符合给定模式 pattern 的 key 。
KEYS * 匹配数据库中所有 key 。
KEYS h?llo 匹配 hello ， hallo 和 hxllo 等。
KEYS h*llo 匹配 hllo 和 heeeeello 等。
KEYS h[ae]llo 匹配 hello 和 hallo ，但不匹配 hillo 。
特殊符号用 \ 隔开
例如：
127.0.0.1:6379> MSET one 1 two 2 three 3 four 4  # 一次设置 4 个 key
OK
127.0.0.1:6379> KEYS *o*
1) "four"
2) "two"
3) "one"
127.0.0.1:6379> KEYS t??
1) "two"
127.0.0.1:6379> KEYS t[w]*
1) "two"
127.0.0.1:6379> KEYS *  # 匹配数据库内所有 key
1) "four"
2) "three"
3) "two"
4) "one"
#################
5.RANDOMKEY
从当前数据库中随机返回(不删除)一个 key 
当数据库不为空时，返回一个 key 。
当数据库为空时，返回 nil 。
例如：
127.0.0.1:6379> mset one two three four five six # 设置多个 key
OK
127.0.0.1:6379> randomkey
"five"
127.0.0.1:6379> randomkey
"six"
127.0.0.1:6379> randomkey
"set"
127.0.0.1:6379> keys * # 查看数据库内所有key，证明 RANDOMKEY 并不删除 key
 1) "greeting"
 2) "tag"
 3) "one"
 4) "six"
127.0.0.1:6379> flushdb # 删除当前数据库所有 key
OK
127.0.0.1:6379> keys * 
(empty list or set)
##################
6. TYPE
返回 key 所储存的值的类型。
none (key不存在)
string (字符串)
list (列表)
set (集合)
zset (有序集)
hash (哈希表)
例如：
127.0.0.1:6379> set 127.0.0.1:6379 baidu
OK
127.0.0.1:6379> type 127.0.0.1:6379 # 字符串
string
127.0.0.1:6379> lpush web welcome to beijing
(integer) 3
127.0.0.1:6379> type web # 列表
list
127.0.0.1:6379> sadd hb jfedu
(integer) 1
127.0.0.1:6379> type hb # 集合
set
##################### 
String（字符串）先看DECR DECRBY GET INCR INCRBY 
INCRBYFLOAT MGET MSET SET STRLEN 
1.DECR
将 key 中储存的数字值减一。
如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 DECR 操作。
如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误
#执行 DECR 命令之后 key 的值。
例如：
127.0.0.1:6379> set db 10 # 对存在的数字值 key 进行 DECR
OK
127.0.0.1:6379> decr db
(integer) 9
127.0.0.1:6379> exists web # 对不存在的 key 值进行 DECR
(integer) 0
127.0.0.1:6379> decr web 
(integer) -1 
127.0.0.1:6379> set web 127.0.0.1:6379 # 对存在但不是数值的 key 进行 DECR
OK
127.0.0.1:6379> get web
"127.0.0.1:6379"
127.0.0.1:6379> decr web
(error) ERR value is not an integer or out of range
============
2. DECRBY 
将 key 所储存的值减去减量 decrement 。
如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 DECRBY 操作。
如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误。
#减去 decrement 之后， key 的值。
例如：
127.0.0.1:6379> set db 40 # 对已存在的 key 进行 DECRBY
OK
127.0.0.1:6379> decrby db 10
(integer) 30
127.0.0.1:6379> exists hello # 对不存在的 key 进行DECRBY
(integer) 0
127.0.0.1:6379> decrby hello 20
(integer) -20
=======
3. GET
返回 key 所关联的字符串值。
如果 key 不存在那么返回特殊值 nil 。
假如 key 储存的值不是字符串类型，返回一个错误，因为 GET 只能用于处理字符串值。
#当 key 不存在时，返回 nil ，否则，返回 key 的值。
#如果 key 不是字符串类型，那么返回一个错误。
例如：
127.0.0.1:6379> get 127.0.0.1:6379 # 对不存在的 key 或字符串类型 key 进行 GET
(nil)
127.0.0.1:6379> set 127.0.0.1:6379 baidi.com
OK
127.0.0.1:6379> get 127.0.0.1:6379
"baidi.com"
127.0.0.1:6379> del 127.0.0.1:6379 # 对不是字符串类型的 key 进行 GET
(integer) 1
127.0.0.1:6379> lpush 127.0.0.1:6379 test mysql
(integer) 2
127.0.0.1:6379> get 127.0.0.1:6379
(error) WRONGTYPE Operation against a key holding the wrong kind of value
============
4.INCR
将 key 中储存的数字值增一。
如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 INCR 操作。
如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误。
#执行 INCR 命令之后 key 的值。
例如：
127.0.0.1:6379> set page 30
OK
127.0.0.1:6379> get page
"30"
127.0.0.1:6379> incr page
(integer) 31
127.0.0.1:6379> get page # 数字值在 127.0.0.1:6379 中以字符串的形式保存
"31"
============
5.INCRBY
将 key 所储存的值加上增量 increment 。
如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 INCRBY 命令。
如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误
#加上 increment 之后， key 的值。
例如：
127.0.0.1:6379> set mysql 20 # key 存在且是数字值
OK
127.0.0.1:6379> get mysql
"20"
127.0.0.1:6379> incrby mysql 30
(integer) 50
127.0.0.1:6379> get mysql
"50"
127.0.0.1:6379> exists hello # key 不存在时
(integer) 0
127.0.0.1:6379> incrby beijin 60
(integer) 60
127.0.0.1:6379> get beijin
"60"
127.0.0.1:6379> set book welcome # key 不是数字值时
OK
127.0.0.1:6379> get book 
"welcome"
127.0.0.1:6379> incrby book 40
(error) ERR value is not an integer or out of range
===========
6.INCRBYFLOAT 
为 key 中所储存的值加上浮点数增量 increment 。
如果 key 不存在，那么 INCRBYFLOAT 会先将 key 的值设为 0 ，再执行加法操作。
如果命令执行成功，那么 key 的值会被更新为（执行加法之后的）新值，
并且新值会以字符串的形式返回给调用者。
无论是 key 的值，还是增量 increment ，都可以使用像 2.0e7 、 3e5 、
90e-2 那样的指数符号(exponential notation)来表示，
但是，执行 INCRBYFLOAT 命令之后的值总是以同样的形式储存，
也即是，它们总是由一个数字，一个（可选的）
小数点和一个任意位的小数部分组成（比如 3.14 、 69.768 ，诸如此类)，
小数部分尾随的 0 会被移除，如果有需要的话，还会将浮点数改为整数
（比如 3.0 会被保存成 3 ）。
除此之外，无论加法计算所得的浮点数的实际精度有多长，
INCRBYFLOAT 的计算结果也最多只能表示小数点的后十七位。
#执行命令之后 key 的值。
例如：
127.0.0.1:6379> SET mykey 10.50 # 值和增量都不是指数符号
OK
127.0.0.1:6379> INCRBYFLOAT mykey 0.1
"10.6"
127.0.0.1:6379> SET mykey 314e-2 # 值和增量都是指数符号
OK
127.0.0.1:6379> GET mykey  # 用 SET 设置的值可以是指数符号             
"314e-2"
127.0.0.1:6379> INCRBYFLOAT mykey 0  # 但执行 INCRBYFLOAT 之后格式会被改成非指数符号    
"3.14"
127.0.0.1:6379> SET mykey 3 # 可以对整数类型执行
OK
127.0.0.1:6379> INCRBYFLOAT mykey 1.1
"4.1"
127.0.0.1:6379> SET mykey 3.0 # 后跟的 0 会被移除
OK
127.0.0.1:6379> GET mykey  # SET 设置的值小数部分可以是 0
"3.0"
127.0.0.1:6379> INCRBYFLOAT mykey 1.000000000000000000000 # 但 INCRBYFLOAT 会将无用的 0 忽略掉，有需要的话，将浮点变为整数  
"4"
127.0.0.1:6379> GET mykey
"4"
=========
7.MGET 
返回所有(一个或多个)给定 key 的值。
如果给定的 key 里面，有某个 key 不存在，那么这个 key 返回特殊值 nil 。
因此，该命令永不失败。
#一个包含所有给定 key 的值的列表。
例如：
127.0.0.1:6379> SET 127.0.0.1:6379 127.0.0.1:6379.com
OK
127.0.0.1:6379> SET mongodb mongodb.com
OK
127.0.0.1:6379> MGET 127.0.0.1:6379 mongodb
1) "127.0.0.1:6379.com"
2) "mongodb.com"
127.0.0.1:6379> MGET 127.0.0.1:6379 mongodb mysql  # 不存在的 mysql 返回 nil
1) "127.0.0.1:6379.com"
2) "mongodb.com"
3) (nil)
=========
8.MSET 
同时设置一个或多个 key-value 对。
如果某个给定 key 已经存在，那么 MSET 会用新值覆盖原来的旧值，
如果这不是你所希望的效果，请考虑使用 MSETNX 命令：
它只会在所有给定 key 都不存在的情况下进行设置操作。
MSET 是一个原子性(atomic)操作，所有给定 key 都会在同一时间内被设置，
某些给定 key 被更新而另一些给定 key 没有改变的情况，不可能发生。
#总是返回 OK (因为 MSET 不可能失败)
例如：
127.0.0.1:6379> MSET date "2012" time "11:00" weather "sunny"
OK
127.0.0.1:6379> MGET date time weather
1) "2012"
2) "11:00"
3) "sunny"
127.0.0.1:6379> SET google "google.hk" # MSET 覆盖旧值例子
OK
127.0.0.1:6379> MSET google "google.com"
OK
127.0.0.1:6379> GET google
"google.com"
=====
9.SET
将字符串值 value 关联到 key 。
如果 key 已经持有其他值， SET 就覆写旧值，无视类型。
对于某个原本带有生存时间（TTL）的键来说，
当 SET 命令成功在这个键上执行时， 这个键原有的 TTL 将被清除。
#在 127.0.0.1:6379 2.6.12 版本以前， SET 命令总是返回 OK 。
从 127.0.0.1:6379 2.6.12 版本开始， SET 在设置操作成功完成时，才返回 OK 。
如果设置了 NX 或者 XX ，但因为条件没达到而造成设置操作未执行， 
那么命令返回空批量回复（NULL Bulk Reply）
例如：
27.0.0.1:6379> SET key "value" # 对不存在的键进行设置
OK
127.0.0.1:6379> GET key
"value"
127.0.0.1:6379> SET key "new-value" # 对已存在的键进行设置
OK
127.0.0.1:6379> GET key
"new-value"
========
10.STRLEN 
返回 key 所储存的字符串值的长度。
当 key 储存的不是字符串值时，返回一个错误。
#字符串值的长度。当 key 不存在时，返回 0 。
例如：
127.0.0.1:6379> SET mykey "Hello world" # 获取字符串的长度
OK
127.0.0.1:6379> STRLEN mykey
(integer) 11
127.0.0.1:6379> STRLEN sting # 不存在的 key 长度为 0
(integer) 0
============
Hash（哈希表）
1. HGET 
返回哈希表 key 中给定域 field 的值。
#给定域的值。
#当给定域不存在或是给定 key 不存在时，返回 nil 。
例如：
127.0.0.1:6379> HSET site 127.0.0.1:6379 127.0.0.1:6379.com # 域存在
(integer) 1
127.0.0.1:6379> HGET site 127.0.0.1:6379
"127.0.0.1:6379.com"
127.0.0.1:6379> HGET site mysql # 域不存在
(nil)
========
2.HGETALL
返回哈希表 key 中，所有的域和值。
在返回值里，紧跟每个域名(field name)之后是域的值(value)，
所以返回值的长度是哈希表大小的两倍。
#以列表形式返回哈希表的域和域的值。
#若 key 不存在，返回空列表。
例如：
127.0.0.1:6379> HSET people jack "Jack Sparrow"
(integer) 1
127.0.0.1:6379> HSET people gump "Forrest Gump"
(integer) 1
127.0.0.1:6379> HGETALL people
1) "jack"          # 域
2) "Jack Sparrow"  # 值
3) "gump"
4) "Forrest Gump"
================
3 .HSET HKEYS
将哈希表 key 中的域 field 的值设为 value 。
如果 key 不存在，一个新的哈希表被创建并进行 HSET 操作。
如果域 field 已经存在于哈希表中，旧值将被覆盖。

#如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1 。
#如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0 。
例如：
127.0.0.1:6379> HSET website google "www.g.cn"       # 设置一个新域
(integer) 1
127.0.0.1:6379> HSET website google "www.google.com" # 覆盖一个旧域
(integer) 0
===========
5.HKEYS
返回哈希表 key 中的所有域。
#一个包含哈希表中所有域的表。
#当 key 不存在时，返回一个空表。
例如：
# 哈希表非空
127.0.0.1:6379> HMSET website google www.google.com yahoo www.yahoo.com # 哈希表非空
OK
127.0.0.1:6379> HKEYS website
1) "google"
2) "yahoo"
127.0.0.1:6379> EXISTS fake_key # 空哈希表/key不存在
(integer) 0
127.0.0.1:6379> HKEYS fake_key
(empty list or set)
=============
List（列表）
1. LLEN 
返回列表 key 的长度。
如果 key 不存在，则 key 被解释为一个空列表，返回 0 .
如果 key 不是列表类型，返回一个错误。
#列表 key 的长度。
例如：
127.0.0.1:6379> LLEN job # 空列表
(integer) 0
127.0.0.1:6379> LPUSH job "cook food" # 非空列表
(integer) 1
127.0.0.1:6379> LPUSH job "have lunch"
(integer) 2
127.0.0.1:6379> LLEN job
(integer) 2
========
2. LPUSH
将一个或多个值 value 插入到列表 key 的表头
如果有多个 value 值，那么各个 value 值按从左到右的顺序依次插入到表头： 
比如说，对空列表 mylist 执行命令 LPUSH mylist a b c ，
列表的值将是 c b a ，这等同于原子性地执行 LPUSH mylist a 、
 LPUSH mylist b 和 LPUSH mylist c 三个命令。
如果 key 不存在，一个空列表会被创建并执行 LPUSH 操作。
当 key 存在但不是列表类型时，返回一个错误。
#执行 LPUSH 命令后，列表的长度。
例如：
127.0.0.1:6379> LPUSH languages python # 加入单个元素
(integer) 1
127.0.0.1:6379> LPUSH languages python # 加入重复元素
(integer) 2
127.0.0.1:6379> LRANGE languages 0 -1  # 列表允许重复元素
1) "python"
2) "python"
127.0.0.1:6379> LPUSH mylist a b c  # 加入多个元素
(integer) 3
127.0.0.1:6379> LRANGE mylist 0 -1
1) "c"
2) "b"
3) "a"
=======
3. LPOP
移除并返回列表 key 的头元素。
#列表的头元素。当 key 不存在时，返回 nil 。
例如：
127.0.0.1:6379> LLEN course
(integer) 0
127.0.0.1:6379> RPUSH course algorithm001
(integer) 1
127.0.0.1:6379> RPUSH course c++101
(integer) 2
127.0.0.1:6379> LPOP course  # 移除头元素
"algorithm001"
=======
4. LRANGE
返回列表 key 中指定区间内的元素，区间以偏移量 start 和 stop 指定。
下标(index)参数 start 和 stop 都以 0 为底
也就是说，以 0 表示列表的第一个元素，以 1 表示列表的第二个元素，以此类推。
你也可以使用负数下标，以 -1 表示列表的最后一个元素，
 -2 表示列表的倒数第二个元素，以此类推。
#一个列表，包含指定区间内的元素。
例如：
127.0.0.1:6379> RPUSH fp-language lisp
(integer) 1
127.0.0.1:6379> LRANGE fp-language 0 0
1) "lisp"
127.0.0.1:6379> RPUSH fp-language scheme
(integer) 2
127.0.0.1:6379> LRANGE fp-language 0 1
1) "lisp"
2) "scheme"
=========
5. LREM
根据参数 count 的值，移除列表中与参数 value 相等的元素。
count 的值可以是以下几种：
count > 0 : 从表头开始向表尾搜索，移除与 value 相等的元素，数量为 count 。
count < 0 : 从表尾开始向表头搜索，移除与 value 相等的元素，数量为 count 的绝对值。
count = 0 : 移除表中所有与 value 相等的值。
#被移除元素的数量。
#因为不存在的 key 被视作空表(empty list)，所以当 key 不存在时， LREM 命令总是返回 0 。
例如：
# morning hello morning helllo morning # 先创建一个表，内容排列是
127.0.0.1:6379> LPUSH greet "morning"
(integer) 1
127.0.0.1:6379> LPUSH greet "hello"
(integer) 2
127.0.0.1:6379> LPUSH greet "morning"
(integer) 3
127.0.0.1:6379> LPUSH greet "hello"
(integer) 4
127.0.0.1:6379> LPUSH greet "morning"
(integer) 5
127.0.0.1:6379> LRANGE greet 0 4 # 查看所有元素
1) "morning"
2) "hello"
3) "morning"
4) "hello"
5) "morning"
127.0.0.1:6379> LREM greet 2 morning   # 移除从表头到表尾，最先发现的两个 morning
(integer) 2             # 两个元素被移除
127.0.0.1:6379> LLEN greet   # 还剩 3 个元素
(integer) 3
127.0.0.1:6379> LRANGE greet 0 2
1) "hello"
2) "hello"
3) "morning"
127.0.0.1:6379> LREM greet -1 morning    # 移除从表尾到表头，第一个 morning
(integer) 1
127.0.0.1:6379> LLEN greet # 剩下两个元素
(integer) 2
127.0.0.1:6379> LRANGE greet 0 1
1) "hello"
2) "hello"
127.0.0.1:6379> LREM greet 0 hello   # 移除表中所有 hello
(integer) 2        # 两个 hello 被移除
127.0.0.1:6379> LLEN greet
(integer) 0
=====
6. LTRIM
对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，
不在指定区间之内的元素都将被删除。
举个例子，执行命令 LTRIM list 0 2 ，表示只保留列表 list 的前三个元素，其余元素全部删除。
下标(index)参数 start 和 stop 都以 0 为底，也就是说，以 0 表示列表的第一个元素，
以 1 表示列表的第二个元素，以此类推。
你也可以使用负数下标，以 -1 表示列表的最后一个元素，
 -2 表示列表的倒数第二个元素，以此类推。
当 key 不是列表类型时，返回一个错误。
LTRIM 命令通常和 LPUSH 命令或 RPUSH 命令配合使用，举个例子：
LPUSH log newest_log
LTRIM log 0 99
这个例子模拟了一个日志程序，每次将最新日志 newest_log 放到 log 列表中，
并且只保留最新的 100 项。注意当这样使用 LTRIM 命令时，时间复杂度是O(1)，
因为平均情况下，每次只有一个元素被移除。
注意LTRIM命令和编程语言区间函数的区别
假如你有一个包含一百个元素的列表 list ，对该列表执行 LTRIM list 0 10 ，
结果是一个包含11个元素的列表，这表明 stop 下标也在 LTRIM 命令的取值范围之内(闭区间)，
这和某些语言的区间函数可能不一致，
比如Ruby的 Range.new 、 Array#slice 和Python的 range() 函数。
超出范围的下标
超出范围的下标值不会引起错误。
如果 start 下标比列表的最大下标 end ( LLEN list 减去 1 )还要大，
或者 start > stop ， LTRIM 返回一个空列表(因为 LTRIM 已经将整个列表清空)。
如果 stop 下标比 end 下标还要大，Redis将 stop 的值设置为 end 。
#命令执行成功时，返回 ok 。
例如：
# 情况 1： 常见情况， start 和 stop 都在列表的索引范围之内
127.0.0.1:6379> LRANGE alpha 0 -1       # alpha 是一个包含 5 个字符串的列表
1) "h"
2) "e"
3) "l"
4) "l"
5) "o"
127.0.0.1:6379> LTRIM alpha 1 -1        # 删除 alpha 列表索引为 0 的元素
OK
127.0.0.1:6379> LRANGE alpha 0 -1       # "h" 被删除了
1) "e"
2) "l"
3) "l"
4) "o"
# 情况 2： stop 比列表的最大下标还要大
127.0.0.1:6379> LTRIM alpha 1 10086     # 保留 alpha 列表索引 1 至索引 10086 上的元素
OK
127.0.0.1:6379> LRANGE alpha 0 -1       # 只有索引 0 上的元素 "e" 被删除了，其他元素还在
1) "l"
2) "l"
3) "o"
# 情况 3： start 和 stop 都比列表的最大下标要大，并且 start < stop
127.0.0.1:6379> LTRIM alpha 10086 123321
OK
127.0.0.1:6379> LRANGE alpha 0 -1        # 列表被清空
(empty list or set)
# 情况 4： start 和 stop 都比列表的最大下标要大，并且 start > stop
127.0.0.1:6379> RPUSH new-alpha "h" "e" "l" "l" "o"     # 重新建立一个新列表
(integer) 5
127.0.0.1:6379> LRANGE new-alpha 0 -1
1) "h"
2) "e"
3) "l"
4) "l"
5) "o"
127.0.0.1:6379> LTRIM new-alpha 123321 10086    # 执行 LTRIM
OK
127.0.0.1:6379> LRANGE new-alpha 0 -1           # 同样被清空
(empty list or set)
==========
7. RPOP
移除并返回列表 key 的尾元素。
#列表的尾元素。
当 key 不存在时，返回 nil 
例如：
127.0.0.1:6379> RPUSH mylist "one"
(integer) 1
127.0.0.1:6379> RPUSH mylist "two"
(integer) 2
127.0.0.1:6379> RPUSH mylist "three"
(integer) 3
127.0.0.1:6379> RPOP mylist           # 返回被弹出的元素
"three"
127.0.0.1:6379> LRANGE mylist 0 -1    # 列表剩下的元素
1) "one"
2) "two"
========
8. RPUSH.
将一个或多个值 value 插入到列表 key 的表尾(最右边)。
如果有多个 value 值，那么各个 value 值按从左到右的顺序依次插入到表尾：
比如对一个空列表 mylist 执行 RPUSH mylist a b c ，
得出的结果列表为 a b c ，等同于执行命令 RPUSH mylist a 、
 RPUSH mylist b 、 RPUSH mylist c 。
如果 key 不存在，一个空列表会被创建并执行 RPUSH 操作。
当 key 存在但不是列表类型时，返回一个错误。
例如：
127.0.0.1:6379> RPUSH languages c # 添加单个元素
(integer) 1
# 添加重复元素
127.0.0.1:6379> RPUSH languages c
(integer) 2
127.0.0.1:6379> LRANGE languages 0 -1 # 列表允许重复元素
1) "c"
2) "c"
# 添加多个元素
127.0.0.1:6379> RPUSH mylist a b c
(integer) 3
127.0.0.1:6379> LRANGE mylist 0 -1
1) "a"
2) "b"
3) "c"
==========
Set（集合）
1.SADD
将一个或多个 member 元素加入到集合 key 当中，已经存在于集合的 member 元素将被忽略。
假如 key 不存在，则创建一个只包含 member 元素作成员的集合。
当 key 不是集合类型时，返回一个错误
#被添加到集合中的新元素的数量，不包括被忽略的元素。
例如：
# 添加单个元素
127.0.0.1:6379> SADD bbs "discuz.net" # 添加单个元素
(integer) 1
# 添加重复元素
127.0.0.1:6379> SADD bbs "discuz.net"
(integer) 0
# 添加多个元素
127.0.0.1:6379> SADD bbs "tianya.cn" "groups.google.com"
(integer) 2
127.0.0.1:6379> SMEMBERS bbs
1) "discuz.net"
2) "groups.google.com"
3) "tianya.cn"
===
2. SINTER 
返回一个集合的全部成员，该集合是所有给定集合的交集。
不存在的 key 被视为空集。
当给定集合当中有一个空集时，结果也为空集(根据集合运算定律)。
#交集成员的列表。
127.0.0.1:6379> SMEMBERS group_1
1) "LI LEI"
2) "TOM"
3) "JACK"
127.0.0.1:6379> SMEMBERS group_2
1) "HAN MEIMEI"
2) "JACK"
127.0.0.1:6379> SINTER group_1 group_2
1) "JACK"
===
3.SPOP
移除并返回集合中的一个随机元素。
如果只想获取一个随机元素，但不想该元素从集合中被移除的话，可以使用 SRANDMEMBER 命令。
#被移除的随机元素。当 key 不存在或 key 是空集时，返回 nil 。
例如：
127.0.0.1:6379> SMEMBERS db
1) "MySQL"
2) "MongoDB"
3) "127.0.0.1:6379"
127.0.0.1:6379> SPOP db
"127.0.0.1:6379"
127.0.0.1:6379> SMEMBERS db
1) "MySQL"
2) "MongoDB"
127.0.0.1:6379> SPOP db
"MySQL"
127.0.0.1:6379> SMEMBERS db
1) "MongoDB"
=====
4.SRANDMEMBER
如果命令执行时，只提供了 key 参数，那么返回集合中的一个随机元素。
#只提供 key 参数时，返回一个元素；如果集合为空，返回 nil 。
如果提供了 count 参数，那么返回一个数组；如果集合为空，返回空数组。
例如：
127.0.0.1:6379> SADD fruit apple banana cherry # 添加元素
(integer) 3
# 只给定 key 参数，返回一个随机元素
127.0.0.1:6379> SRANDMEMBER fruit
"cherry"
127.0.0.1:6379> SRANDMEMBER fruit
"apple"
# 给定 3 为 count 参数，返回 3 个随机元素
# 每个随机元素都不相同
127.0.0.1:6379> SRANDMEMBER fruit 3
1) "apple"
2) "banana"
3) "cherry"
# 给定 -3 为 count 参数，返回 3 个随机元素
# 元素可能会重复出现多次
127.0.0.1:6379> SRANDMEMBER fruit -3
1) "banana"
2) "cherry"
3) "apple"
127.0.0.1:6379> SRANDMEMBER fruit -3
1) "apple"
2) "apple"
3) "cherry"
# 如果 count 是整数，且大于等于集合基数，那么返回整个集合
127.0.0.1:6379> SRANDMEMBER fruit 10
1) "apple"
2) "banana"
3) "cherry"
# 如果 count 是负数，且 count 的绝对值大于集合的基数
# 那么返回的数组的长度为 count 的绝对值
127.0.0.1:6379> SRANDMEMBER fruit -10
1) "banana"
2) "apple"
3) "banana"
4) "cherry"
5) "apple"
6) "apple"
7) "cherry"
8) "apple"
9) "apple"
10) "banana"
# SRANDMEMBER 并不会修改集合内容
127.0.0.1:6379> SMEMBERS fruit
1) "apple"
2) "cherry"
3) "banana"
# 集合为空时返回 nil 或者空数组
127.0.0.1:6379> SRANDMEMBER not-exists
(nil)
127.0.0.1:6379> SRANDMEMBER not-eixsts 10
(empty list or set)
======
5. SREM
移除集合 key 中的一个或多个 member 元素，不存在的 member 元素会被忽略。
当 key 不是集合类型，返回一个错误。
#被成功移除的元素的数量，不包括被忽略的元素。
例如：
127.0.0.1:6379> SMEMBERS languages # 测试数据
1) "c"
2) "lisp"
3) "python"
4) "ruby"
# 移除单个元素
127.0.0.1:6379> SREM languages ruby
(integer) 1
# 移除不存在元素
127.0.0.1:6379> SREM languages non-exists-language
(integer) 0
# 移除多个元素
127.0.0.1:6379> SREM languages lisp python c
(integer) 3
127.0.0.1:6379> SMEMBERS languages
(empty list or set)
======
SortedSet（有序集合）
1. ZADD
将一个或多个 member 元素及其 score 值加入到有序集 key 当中。
如果某个 member 已经是有序集的成员，那么更新这个 member 的 score 值，
并通过重新插入这个 member 元素，来保证该 member 在正确的位置上。
score 值可以是整数值或双精度浮点数。
如果 key 不存在，则创建一个空的有序集并执行 ZADD 操作。
当 key 存在但不是有序集类型时，返回一个错误。
对有序集的更多介绍请参见 sorted set 。
例如：
127.0.0.1:6379> ZADD page_rank 10 google.com # 添加单个元素
(integer) 1
# 添加多个元素
127.0.0.1:6379> ZADD page_rank 9 baidu.com 8 bing.com
(integer) 2
127.0.0.1:6379> ZRANGE page_rank 0 -1 WITHSCORES
1) "bing.com"
2) "8"
3) "baidu.com"
4) "9"
5) "google.com"
6) "10"
# 添加已存在元素，且 score 值不变
127.0.0.1:6379> ZADD page_rank 10 google.com
(integer) 0
127.0.0.1:6379> ZRANGE page_rank 0 -1 WITHSCORES  # 没有改变
1) "bing.com"
2) "8"
3) "baidu.com"
4) "9"
5) "google.com"
6) "10"
# 添加已存在元素，但是改变 score 值
127.0.0.1:6379> ZADD page_rank 6 bing.com
(integer) 0
127.0.0.1:6379> ZRANGE page_rank 0 -1 WITHSCORES  # bing.com 元素的 score 值被改变
1) "bing.com"
2) "6"
3) "baidu.com"
4) "9"
5) "google.com"
6) "10"
======
2. ZCOUNT
返回有序集 key 中， score 值在 min 和
 max 之间(默认包括 score 值等于 min 或 max )的成员的数量。
关于参数 min 和 max 的详细使用方法，请参考 ZRANGEBYSCORE 命令。
#score 值在 min 和 max 之间的成员的数量。
例如：
127.0.0.1:6379> ZRANGE salary 0 -1 WITHSCORES    # 测试数据
1) "jack"
2) "2000"
3) "peter"
4) "3500"
5) "tom"
6) "5000"
127.0.0.1:6379> ZCOUNT salary 2000 5000  # 计算薪水在 2000-5000 之间的人数
(integer) 3
127.0.0.1:6379> ZCOUNT salary 3000 5000  # 计算薪水在 3000-5000 之间的人数
(integer) 2
========
3.ZINCRBY
为有序集 key 的成员 member 的 score 值加上增量 increment 。
可以通过传递一个负数值 increment ，让 score 减去相应的值，
比如 ZINCRBY key -5 member ，就是让 member 的 score 值减去 5 。
当 key 不存在，或 member 不是 key 的成员时， 
ZINCRBY key increment member 等同于 ZADD key increment member 。
当 key 不是有序集类型时，返回一个错误。
score 值可以是整数值或双精度浮点数。
#member 成员的新 score 值，以字符串形式表示。
例如：
127.0.0.1:6379> ZSCORE salary tom
"2000"
127.0.0.1:6379> ZINCRBY salary 2000 tom   # tom 加薪啦！
"4000"
===
4.ZRANGE
返回有序集 key 中，指定区间内的成员。
其中成员的位置按 score 值递增(从小到大)来排序。
具有相同 score 值的成员按字典序(lexicographical order )来排列。
如果你需要成员按 score 值递减(从大到小)来排列，请使用 ZREVRANGE 命令。
下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，
以 1 表示有序集第二个成员，以此类推。
你也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推。
超出范围的下标并不会引起错误。
比如说，当 start 的值比有序集的最大下标还要大，或是 start > stop 时，
 ZRANGE 命令只是简单地返回一个空列表。
另一方面，假如 stop 参数的值比有序集的最大下标还要大，那么 127.0.0.1:6379
 将 stop 当作最大下标来处理。
可以通过使用 WITHSCORES 选项，来让成员和它的 score 值一并返回，
#指定区间内，带有 score 值(可选)的有序集成员的列表。
例如：
127.0.0.1:6379 > ZRANGE salary 0 -1 WITHSCORES   # 显示整个有序集成员
1) "jack"
2) "3500"
3) "tom"
4) "5000"
5) "boss"
6) "10086"
127.0.0.1:6379 > ZRANGE salary 1 2 WITHSCORES  # 显示有序集下标区间 1 至 2 的成员
1) "tom"
2) "5000"
3) "boss"
4) "10086"
127.0.0.1:6379 > ZRANGE salary 0 200000 WITHSCORES   # 测试 end 下标超出最大下标时的情况
1) "jack"
2) "3500"
3) "tom"
4) "5000"
5) "boss"
6) "10086"
127.0.0.1:6379 > ZRANGE salary 200000 3000000 WITHSCORES   # 测试当给定区间不存在于有序集时的情况
(empty list or set)
===========
5. ZRANK
返回有序集 key 中成员 member 的排名。其中有序集成员按 score 值递增(从小到大)顺序排列。
排名以 0 为底，也就是说， score 值最小的成员排名为 0 
使用 ZREVRANK 命令可以获得成员按 score 值递减(从大到小)排列的排名。
#如果 member 是有序集 key 的成员，返回 member 的排名。
如果 member 不是有序集 key 的成员，返回 nil 。
例如：
127.0.0.1:6379> ZRANGE salary 0 -1 WITHSCORES  # 显示所有成员及其 score 值
1) "peter"
2) "3500"
3) "tom"
4) "4000"
5) "jack"
6) "5000"
127.0.0.1:6379> ZRANK salary tom    # 显示 tom 的薪水排名，第二
(integer) 1
=======
6.ZREVRANGE
返回有序集 key 中，指定区间内的成员。
其中成员的位置按 score 值递减(从大到小)来排列。
具有相同 score 值的成员按字典序的逆序(reverse lexicographical order)排列。
除了成员按 score 值递减的次序排列这一点外， ZREVRANGE 命令的其他方面和 ZRANGE 命令一样。
#指定区间内，带有 score 值(可选)的有序集成员的列表。
例如：
127.0.0.1:6379> ZRANGE salary 0 -1 WITHSCORES        # 递增排列
1) "peter"
2) "3500"
3) "tom"
4) "4000"
5) "jack"
6) "5000"
127.0.0.1:6379> ZREVRANGE salary 0 -1 WITHSCORES     # 递减排列
1) "jack"
2) "5000"
3) "tom"
4) "4000"
5) "peter"
6) "3500"
=====
7.ZCARD 
返回有序集 key 的基数。
#当 key 存在且是有序集类型时，返回有序集的基数。
当 key 不存在时，返回 0 。
例如：
127.0.0.1:6379 > ZADD salary 2000 tom    # 添加一个成员
(integer) 1
127.0.0.1:6379 > ZCARD salary
(integer) 1
127.0.0.1:6379 > ZADD salary 5000 jack   # 再添加一个成员
(integer) 1
127.0.0.1:6379 > ZCARD salary
(integer) 2
127.0.0.1:6379 > EXISTS non_exists_key   # 对不存在的 key 进行 ZCARD 操作
(integer) 0
127.0.0.1:6379 > ZCARD non_exists_key
(integer) 0
